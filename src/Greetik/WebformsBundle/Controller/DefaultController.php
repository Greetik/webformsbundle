<?php

namespace Greetik\WebformsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\WebformsBundle\Entity\Formfield;
use Greetik\WebformsBundle\Entity\Formfieldoption;
use Symfony\Component\Config\Definition\Exception\Exception;
use Greetik\WebformsBundle\Form\Type\FormfieldsType;
use Greetik\WebformsBundle\Form\Type\FormfieldoptionType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;
use Greetik\WebformsBundle\Form\Type\FormconfigType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    public function indexAction($idproject = '') {

        $config = $this->get('webforms.tools')->getFormconfig($idproject);
        return $this->render($this->getParameter('webforms.interface') . ':index.html.twig', array_merge(array(
                    'data' => $this->get($this->getParameter('webforms.permsservice'))->getFormfieldsByProject($idproject),
                    'idproject' => $idproject,
                    'insertAllow' => $this->get($this->getParameter('webforms.permsservice'))->getFormfieldPerm('insert', '')), ((!$config->getIgnoremail()) ? array('new_form' => $this->createForm(FormconfigType::class, $config)->createView()) : array())
        ));
    }

    /**
     * View an individual formfield, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function viewfieldAction($id) {
        $formfield = $this->get($this->getParameter('webforms.permsservice'))->getFormfield($id);
        $formfieldobject = $this->get('webforms.tools')->getFormfieldObject($id);
        $editForm = $this->createForm(FormfieldsType::class, $formfieldobject);
        return $this->render($this->getParameter('webforms.interface') . ':view.html.twig', array(
                    'item' => $formfield,
                    'new_form' => $editForm->createView(),
                    'idproject' => $formfield['project'],
                    'modifyAllow' => $this->get($this->getParameter('webforms.permsservice'))->getFormfieldPerm($formfieldobject, 'modify')
        ));
    }

    /**
     * Get the data of a new Formfield by Formfield and persis it
     * 
     * @param Formfield $item is received by Formfield Request
     * @author Pacolmg
     */
    public function insertfieldAction(Request $request, $idproject = '') {

        $formfield = new Formfield();
        $newForm = $this->createForm(FormfieldsType::class, $formfield);
        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                try {
                    $this->get($this->getParameter('webforms.permsservice'))->insertFormfield($formfield, $idproject);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            } else {
                $this->addFlash('error', (string) $newForm->getErrors(true, true));
            }
            return $this->redirect($this->generateUrl('webforms_listformfields', array('idproject' => $idproject)));
        }

        return $this->render($this->getParameter('webforms.interface') . ':insert.html.twig', array(
                    'idproject' => $idproject,
                    'new_form' => $newForm->createView()));
    }

    /**
     * Edit the data of an formfield
     * 
     * @param int $id is received by Get Request
     * @param Formfield $item is received by Formfield Request
     * @author Pacolmg
     */
    public function modifyfieldAction(Request $request, $id) {
        $formfield = $this->get('webforms.tools')->getFormfieldObject($id);

        $editForm = $this->createForm(FormfieldsType::class, $formfield);
        $oldposition = $formfield->getNumorder();


        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try {
                    $this->get($this->getParameter('webforms.permsservice'))->modifyFormfield($formfield, $oldposition);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            } else {
                $this->addFlash('error', (string) $editForm->getErrors(true, true));
            }
        }
        return $this->redirect($this->generateUrl('webforms_viewformfield', array('id' => $id)));
    }

    public function deleteAction($id) {
        $formfield = $this->get('webforms.tools')->getFormfieldObject($id);
        $idproject = $formfield->getProject();
        $error = null;

        try {
            $this->get($this->getParameter('webforms.permsservice'))->deleteFormField($formfield);
        } catch (\Exception $e) {
            $error = $e->getMessage();
            $this->get('session')->getFlashBag()->set('error', $error);
            return $this->redirect($this->generateUrl('webforms_viewformfield', array('id' => $id)));
        }
        return $this->redirect($this->generateUrl('webforms_listformfields', array('idproject' => $idproject)));
    }

    /*     * ****** OPTIONS ****************** */

    public function insertformfieldoptionAction($id_field = '', $id = '') {

        if (!empty($id)) {
            $formfieldoption = $this->get($this->getParameter('webforms.permsservice'))->getFormfieldoption($id);
        } else {
            $formfieldoption = new Formfieldoption();
        }

        $newForm = $this->createForm(FormfieldoptionType::class, $formfieldoption);

        return $this->render('WebformsBundle:Options:insert.html.twig', array('new_form' => $newForm->createView(), 'id' => $id, 'id_field' => $id_field));
    }

    public function insertmodifyfieldoptionAction(Request $request, $id_field = '', $id = '') {

        $item = $request->get('Formfieldoption');

        if (!empty($id)) {
            $formfieldoption = $this->get('webforms.tools')->getFormfieldoptionObject($id);
            $editing = true;
        } else {
            $formfieldoption = new Formfieldoption();
            $editing = false;
        }

        $editForm = $this->createForm(FormfieldoptionType::class, $formfieldoption);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if ($editing) {
                try {
                    $this->get($this->getParameter('webforms.permsservice'))->modifyFormfieldoption($formfieldoption);
                } catch (\Exception $e) {
                    return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
                }
            } else {
                try {
                    $formfieldoption->setFormfield($this->get('webforms.tools')->getFormfieldObject($id_field));
                    $this->get($this->getParameter('webforms.permsservice'))->insertFormfieldoption($formfieldoption);
                } catch (\Exception $e) {
                    return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
                }
            }

            $formfield = $this->get($this->getParameter('webforms.permsservice'))->getFormfield($formfieldoption->getFormfield()->getId());
            return $this->render('WebformsBundle:Options:index.html.twig', array('itemid' => $formfield['id'], 'formfieldoptions' => $formfield['formfieldoptions'], 'modifyAllow' => true));
        } else {
            $errors = (string) $editForm->getErrors(true, true);
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $errors)), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error Desconocido')), 200, array('Content-Type' => 'application/json'));
    }

    public function dropfieldoptionAction($id) {
        $formfieldoption = $this->get('webforms.tools')->getFormfieldoptionObject($id);

        try {
            $this->get($this->getParameter('webforms.permsservice'))->deleteFormfieldoption($formfieldoption);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }

        $formfield = $this->get($this->getParameter('webforms.permsservice'))->getFormfield($formfieldoption->getFormfield()->getId());
        return $this->render('WebformsBundle:Options:index.html.twig', array('itemid' => $formfield['id'], 'formfieldoptions' => $formfield['formfieldoptions'], 'modifyAllow' => true));
    }

    public function changesendtoAction(Request $request, $idproject) {
        $formconfig = $this->get('webforms.tools')->getFormconfig($idproject);
        $editForm = $this->createForm(FormconfigType::class, $formconfig);

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try {
                    $this->get($this->getParameter('webforms.permsservice'))->saveConfig($formconfig);
                } catch (\Exception $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }
        return $this->redirect($this->generateUrl('webforms_listformfields', array('idproject' => $idproject)));
    }

}
