<?php
    namespace Greetik\WebformsBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Doctrine\ORM\EntityRepository;
    use Symfony\Component\Form\Extension\Core\Type\EmailType;
    use Symfony\Component\OptionsResolver\OptionsResolver;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SendtoType
 *
 * @author Paco
 */
class FormconfigType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        
        $builder
            ->add('email', EmailType::class)
            ->add('subject')
            ->add('autores')
            ->add('textres')
                ;
        
                            
    }
    
    public function getName(){
        return 'Formconfig';
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array('data_class' => 'Greetik\WebformsBundle\Entity\Formconfig'
        ));
    }
}

