<?php

namespace Greetik\WebformsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Greetik\ContactformBundle\Entity\Formfieldtype;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ContactformType
 *
 * @author Paco
 */
class FormfieldsType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('formfieldtype')
                ->add('name')
                ->add('numorder')
                ->add('minlong', TextType::class, array('required' => false))
                ->add('maxlong', TextType::class, array('required' => false))
                ->add('disab')
                ->add('oblig');
    }

    public function getName() {
        return 'Formfield';
    }

    public function getDefaultOptions(array $options) {
        return array('data_class' => 'Greetik\WebformsBundle\Entity\Formfield');
    }

}
