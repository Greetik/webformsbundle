<?php
namespace Greetik\WebformsBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class WebfieldType extends AbstractEnumType
{
    const EMAIL = 1;
    const TEXT = 2;
    const TEXTAREA = 3;
    const CHECKBOX = 4;
    const SELECT = 5;
    const MULTISELECT = 6;
    const MONEY = 7;
    const NUMBER = 8;
    const DATE = 9;
    const DATETIME = 10;
    const RICHTEXT = 11;
    
    protected static $choices = [
        self::EMAIL => 'Email',
        self::TEXT => 'Texto Corto',
        self::TEXTAREA => 'Texto Largo',
        self::CHECKBOX => 'Sí/No',
        self::SELECT => 'Selector',
        self::MULTISELECT => 'Selector Múltiple',
        self::MONEY => 'Dinero',
        self::NUMBER => 'Número',
        self::DATE => 'Fecha',
        self::DATETIME => 'Fecha y Hora',
        self::RICHTEXT => 'Texto Enriquecido'
    ];
    
}