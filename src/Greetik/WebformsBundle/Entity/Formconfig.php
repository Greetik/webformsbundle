<?php

namespace Greetik\WebformsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formconfig
 *
 * @ORM\Table(name="formconfig")
 * @ORM\Entity(repositoryClass="Greetik\WebformsBundle\Repository\FormconfigRepository")
 */
class Formconfig
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="project", type="integer", unique=true)
     */
    private $project;

    /**
     * @var bool
     *
     * @ORM\Column(name="ignoremail", type="boolean")
     */
    private $ignoremail=false;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @var bool
     *
     * @ORM\Column(name="autores", type="boolean")
     */
    private $autores=false;

    /**
     * @var string
     *
     * @ORM\Column(name="textres", type="text", nullable=true)
     */
    private $textres;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set project
     *
     * @param integer $project
     *
     * @return Formconfig
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return int
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Formconfig
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set autores
     *
     * @param boolean $autores
     *
     * @return Formconfig
     */
    public function setAutores($autores)
    {
        $this->autores = $autores;

        return $this;
    }

    /**
     * Get autores
     *
     * @return bool
     */
    public function getAutores()
    {
        return $this->autores;
    }

    /**
     * Set textres
     *
     * @param string $textres
     *
     * @return Formconfig
     */
    public function setTextres($textres)
    {
        $this->textres = $textres;

        return $this;
    }

    /**
     * Get textres
     *
     * @return string
     */
    public function getTextres()
    {
        return $this->textres;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Formconfig
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set ignoremail
     *
     * @param boolean $ignoremail
     *
     * @return Formconfig
     */
    public function setIgnoremail($ignoremail)
    {
        $this->ignoremail = $ignoremail;

        return $this;
    }

    /**
     * Get ignoremail
     *
     * @return boolean
     */
    public function getIgnoremail()
    {
        return $this->ignoremail;
    }
}
