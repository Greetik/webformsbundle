<?php

namespace Greetik\WebformsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formfieldoption
 *
 * @ORM\Table(name="formfieldoption", indexes={
 *      @ORM\Index(name="name", columns={"name"})
 * })
 * @ORM\Entity(repositoryClass="Greetik\WebformsBundle\Repository\FormfieldoptionRepository")
 */
class Formfieldoption
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
    * @ORM\ManyToOne(targetEntity="Formfield")
    * @ORM\JoinColumn(name="formfield", referencedColumnName="id", onDelete="CASCADE")
    */
    private $formfield;    
    
   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Formfieldoption
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set formfield
     *
     * @param \Greetik\WebformsBundle\Entity\Formfield $formfield
     *
     * @return Formfieldoption
     */
    public function setFormfield(\Greetik\WebformsBundle\Entity\Formfield $formfield = null)
    {
        $this->formfield = $formfield;

        return $this;
    }

    /**
     * Get formfield
     *
     * @return \Greetik\WebformsBundle\Entity\Formfield
     */
    public function getFormfield()
    {
        return $this->formfield;
    }
}
