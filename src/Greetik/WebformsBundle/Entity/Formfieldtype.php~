<?php

namespace Greetik\ContactformBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formfieldtype
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Greetik\ContactformBundle\Entity\FormfieldtypeRepository")
 */
class Formfieldtype
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="code", type="integer")
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity="Formfield", mappedBy="formfieldtype")
     */
    private $formfields;        

    public function __construct() {
        $this->formfields = new ArrayCollection();
    }    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Formfieldtype
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Formfieldtype
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set code
     *
     * @param integer $code
     *
     * @return Formfieldtype
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return integer
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add formfield
     *
     * @param \Greetik\ContactformBundle\Entity\Formfield $formfield
     *
     * @return Formfieldtype
     */
    public function addFormfield(\Greetik\ContactformBundle\Entity\Formfield $formfield)
    {
        $this->formfields[] = $formfield;

        return $this;
    }

    /**
     * Remove formfield
     *
     * @param \Greetik\ContactformBundle\Entity\Formfield $formfield
     */
    public function removeFormfield(\Greetik\ContactformBundle\Entity\Formfield $formfield)
    {
        $this->formfields->removeElement($formfield);
    }

    /**
     * Get formfields
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFormfields()
    {
        return $this->formfields;
    }
}
