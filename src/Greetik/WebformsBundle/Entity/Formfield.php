<?php

namespace Greetik\WebformsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Greetik\WebformsBundle\DBAL\Types\WebfieldType;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * Formfield
 *
 * @ORM\Table(name="formfield", indexes={
 *      @ORM\Index(name="name", columns={"name"}),  @ORM\Index(name="oblig", columns={"oblig"}),  @ORM\Index(name="disab", columns={"disab"}),  @ORM\Index(name="project", columns={"project"}),  @ORM\Index(name="numorder", columns={"numorder"})
 * })
 * @ORM\Entity(repositoryClass="Greetik\WebformsBundle\Repository\FormfieldRepository")
 */
class Formfield
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * @var boolean
     *
     * @ORM\Column(name="oblig", type="boolean", nullable=true)
     */
    private $oblig;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disab", type="boolean", nullable=true)
     */
    private $disab;

    /**
     * @var integer
     *
     * @ORM\Column(name="project", type="integer")
     */
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(name="externallink", type="string", length=255, nullable=true)
     */
    private $externallink;

    /**
     * @var integer
     *
     * @ORM\Column(name="numorder", type="integer")
     */
    private $numorder;

    /**
     * @var integer
     *
     * @ORM\Column(name="formfieldtype", type="WebfieldType", nullable=true)
     * @DoctrineAssert\Enum(entity="Greetik\WebformsBundle\DBAL\Types\WebfieldType")   
     */
    private $formfieldtype;
    
    /**
     * @ORM\OneToMany(targetEntity="Formfieldoption", mappedBy="formfield")
     */
    private $formfieldoptions;     
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="maxlong", type="integer", nullable=true)
     */
    private $maxlong;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="minlong", type="integer", nullable=true)
     */
    private $minlong;
    

    public function __construct() {
        $this->formfieldoptions = new ArrayCollection();
    }
    
    



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Formfield
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set oblig
     *
     * @param boolean $oblig
     *
     * @return Formfield
     */
    public function setOblig($oblig)
    {
        $this->oblig = $oblig;

        return $this;
    }

    /**
     * Get oblig
     *
     * @return boolean
     */
    public function getOblig()
    {
        return $this->oblig;
    }

    /**
     * Set disab
     *
     * @param boolean $disab
     *
     * @return Formfield
     */
    public function setDisab($disab)
    {
        $this->disab = $disab;

        return $this;
    }

    /**
     * Get disab
     *
     * @return boolean
     */
    public function getDisab()
    {
        return $this->disab;
    }

    /**
     * Set project
     *
     * @param integer $project
     *
     * @return Formfield
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return integer
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set externallink
     *
     * @param string $externallink
     *
     * @return Formfield
     */
    public function setExternallink($externallink)
    {
        $this->externallink = $externallink;

        return $this;
    }

    /**
     * Get externallink
     *
     * @return string
     */
    public function getExternallink()
    {
        return $this->externallink;
    }

    /**
     * Set numorder
     *
     * @param integer $numorder
     *
     * @return Formfield
     */
    public function setNumorder($numorder)
    {
        $this->numorder = $numorder;

        return $this;
    }

    /**
     * Get numorder
     *
     * @return integer
     */
    public function getNumorder()
    {
        return $this->numorder;
    }

    /**
     * Set formfieldtype
     *
     * @param WebfieldType $formfieldtype
     *
     * @return Formfield
     */
    public function setFormfieldtype($formfieldtype)
    {
        $this->formfieldtype = $formfieldtype;

        return $this;
    }

    /**
     * Get formfieldtype
     *
     * @return WebfieldType
     */
    public function getFormfieldtype()
    {
        return $this->formfieldtype;
    }

    /**
     * Set maxlong
     *
     * @param integer $maxlong
     *
     * @return Formfield
     */
    public function setMaxlong($maxlong)
    {
        $this->maxlong = $maxlong;

        return $this;
    }

    /**
     * Get maxlong
     *
     * @return integer
     */
    public function getMaxlong()
    {
        return $this->maxlong;
    }

    /**
     * Set minlong
     *
     * @param integer $minlong
     *
     * @return Formfield
     */
    public function setMinlong($minlong)
    {
        $this->minlong = $minlong;

        return $this;
    }

    /**
     * Get minlong
     *
     * @return integer
     */
    public function getMinlong()
    {
        return $this->minlong;
    }

    /**
     * Add formfieldoption
     *
     * @param \Greetik\WebformsBundle\Entity\Formfieldoption $formfieldoption
     *
     * @return Formfield
     */
    public function addFormfieldoption(\Greetik\WebformsBundle\Entity\Formfieldoption $formfieldoption)
    {
        $this->formfieldoptions[] = $formfieldoption;

        return $this;
    }

    /**
     * Remove formfieldoption
     *
     * @param \Greetik\WebformsBundle\Entity\Formfieldoption $formfieldoption
     */
    public function removeFormfieldoption(\Greetik\WebformsBundle\Entity\Formfieldoption $formfieldoption)
    {
        $this->formfieldoptions->removeElement($formfieldoption);
    }

    /**
     * Get formfieldoptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFormfieldoptions()
    {
        return $this->formfieldoptions;
    }
}
