<?php

namespace Greetik\WebformsBundle\Services;

use Greetik\WebformsBundle\Entity\Formfield;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Greetik\WebformsBundle\Entity\Formconfig;
use Greetik\WebformsBundle\DBAL\Types\WebfieldType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */
class Webformstools {

    private $em;
    private $interfacetools;
    private $mailer;
    private $recaptchsecret;
    private $httpProtocol;

    public function __construct($_entityManager, $_interfacetools, $mailer, $recaptachasecret, $_httpProtocol='') {
        $this->em = $_entityManager;
        $this->interfacetools = $_interfacetools;
        $this->mailer = $mailer;
        $this->recaptchsecret = $recaptachasecret;
        $this->httpProtocol = ((empty($_httpProtocol))? 'http' : $_httpProtocol);
    }

    public function getFormfieldPerm($perm, $formfield = '', $project = '') {
        return true;
    }

    public function getFormfieldsByProject($project) {
        foreach ($data = $this->em->getRepository('WebformsBundle:Formfield')->getFormfields($project) as $k => $v) {
            $data[$k] = $this->setFormfieldData($v);
        }
        return $data;
    }

    public function getFormfieldObject($id) {
        return $this->em->getRepository('WebformsBundle:Formfield')->findOneById($id);
    }

    public function getFormfield($id) {
        return $this->setFormfieldData($this->em->getRepository('WebformsBundle:Formfield')->getFormfield($id));
    }

    public function setFormfieldData($formfield) {
        $formfield['formfieldtypename'] = ($formfield['formfieldtype']) ? \Greetik\WebformsBundle\DBAL\Types\WebfieldType::getReadableValue($formfield['formfieldtype']): '-';
        $formfield['formfieldoptions'] = $this->getFormfieldoptionsByFormfield($formfield['id']);
        return $formfield;
    }

    public function modifyFormfield($formfield, $oldposition) {
        if ($formfield->getNumorder() != $oldposition) {
            $project_id = $formfield->getProject();
            $this->interfacetools->moveitemElem('WebformsBundle:Formfield', $formfield->getId(), $formfield->getNumorder() - 1, $oldposition, $this->em->getRepository('WebformsBundle:Formfield')->findMaxNumOrderByProject($project_id), 's', $project_id, '');
        } else {
            $this->em->persist($formfield);
            $this->em->flush();
        }
    }

    public function insertFormfield($formfield, $project) {
        $formfield->setProject($project);
        if (!$formfield->getOblig())
            $formfield->setOblig(false);
        if (!$formfield->getDisab())
            $formfield->setDisab(false);

        $formfield->setNumorder($this->em->getRepository('WebformsBundle:Formfield')->findMaxNumOrderByProject($project) + 1);

        $this->em->persist($formfield);
        $this->em->flush();
    }

    public function deleteFormfield($formfield) {
        $project = $formfield->getProject();
        $this->em->remove($formfield);
        $this->em->flush();
        $this->interfacetools->reorderItemElems($this->em->getRepository('WebformsBundle:Formfield')->findBy(array('project' => $project), array('numorder' => 'ASC')));
    }

    public function getFormfieldoptionsByFormfield($formfield) {
        return $this->em->getRepository('WebformsBundle:Formfieldoption')->getFormfieldoptions($formfield);
    }

    public function getFormfieldoption($id) {
        return $this->em->getRepository('WebformsBundle:Formfieldoption')->getFormfieldoption($id);
    }

    public function getFormfieldoptionObject($id) {
        return $this->em->getRepository('WebformsBundle:Formfieldoption')->findOneById($id);
    }

    public function modifyFormfieldoption($formfieldoption) {
        $this->em->persist($formfieldoption);
        $this->em->flush();
    }

    public function insertFormfieldoption($formfieldoption) {
        $this->em->persist($formfieldoption);
        $this->em->flush();
    }

    public function deleteFormfieldoption($formfieldoption) {
        $this->em->remove($formfieldoption);
        $this->em->flush();
    }

    public function getFormconfig($project) {
        $formconfig = $this->em->getRepository('WebformsBundle:Formconfig')->findOneByProject($project);
        if (!$formconfig) {
            $formconfig = new Formconfig();
            $formconfig->setProject($project);
            $formconfig->setIgnoremail(false);
            $formconfig->setAutores(false);
            $formconfig->setEmail('');
            $formconfig->setTextres('');
            $this->em->persist($formconfig);
            $this->em->flush();
        }

        return $formconfig;
    }

    public function saveConfig($formconfig) {
        $this->em->persist($formconfig);
        $this->em->flush();
    }

    public function sendmail($id, $params, $userecaptcha = false) {

        $formconfig = $this->getFormconfig($id);

        if ($userecaptcha == true) {
            $recaptcha = $params->get('g-recaptcha-response');
            $google_url = "https://www.google.com/recaptcha/api/siteverify";
            //$secret= $this->modules->getOptionModule(null, 'recaptchasecret')->getValuevar();
            $ip = $params->getClientIp();
            $url = $google_url . "?secret=" . $this->recaptchsecret . "&response=" . $recaptcha . "&remoteip=" . $ip;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json')); // Assuming you're requesting JSON
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $res = curl_exec($ch);
            $res = json_decode($res, true);
            //reCaptcha success check 
            if (!isset($res['success']) || !$res['success']) {
                throw new \Exception('Captcha Incorrecto');
            }
        }

        $fieldforms = $this->getFormfieldsByProject($id);
        $subject = $formconfig->getSubject();
        $message = 'Desde la url: ' . $params->getUri() . '<br />' . $subject . ': <br />';

        /* $pos = strpos($params->getSchemeAndHttpHost(), 'http://'); */ 
        $pos = 3 + strlen($this->httpProtocol);
        
        $emailfrom = 'info@' . substr($params->getSchemeAndHttpHost(), $pos);
        $value = '';
        foreach ($fieldforms as $fieldform) {
            $value = $params->get('field_' . $fieldform['id']);
            if ($value == '' && $fieldform['oblig'])
                throw new \Exception('El campo ' . $fieldform['name'] . ' es obligatorio');
            switch ($fieldform['formfieldtype']) {
                case WebfieldType::CHECKBOX:
                    $value = (($value == 1) ? 'Sí' : 'No');
                    break;
                case WebfieldType::SELECT:
                    $value = $value['name'];
                    break;

                case WebfieldType::MULTISELECT:
                    $value = '';
                    $values = $params->get('field_' . $fieldform['id'], array());

                    $i = 0;
                    foreach ($values as $v) {
                        $v = $this->getFormfieldoption($v);
                        $value .= (($value == '') ? '' : ((count($values) == $i + 1) ? ' y ' : ', ')) . $v['name'];
                        $i++;
                    }

                    break;

                case WebfieldType::EMAIL: $emailtoanswer = $value;
            }

            $message .= '<strong>' . $fieldform['name'] . '</strong>: ' . $value . '<br />';
        }

        $emailto = $formconfig->getEmail();

        if (!$emailto || $emailto == '')
            throw new \Exception('No hay configurado ningún email para este formulario de contacto');

        //if ($this->env=='dev'){
        $swiftmessage = \Swift_Message::newInstance($subject)
                ->setTo($emailto)
                ->setFrom($emailfrom)
                ->setBody($message, 'text/html')
        ;

        $data = $this->mailer->send($swiftmessage);
        if ($data > 0) {
            if ($formconfig->getAutores() && isset($emailtoanswer)) {
                $this->mailer->send(\Swift_Message::newInstance('Gracias por contactar con nosotros')
                                ->setTo($emailtoanswer)
                                ->setFrom($emailfrom)
                                ->setBody($formconfig->getTextres(), 'text/html')
                );
            }
            return $data;
        }

        throw new \Exception('No se pudo enviar el email, póngase en contacto con nosotros para que podamos solventar el error');
    }

}
